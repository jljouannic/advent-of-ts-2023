import { Expect, Equal } from 'type-testing'

type FindSantaInRow<T extends unknown[]> = T extends '🎄'[]
  ? never
  : T extends ['🎅', ...'🎄'[]]
    ? 0
    : T extends [...infer Trees, '🎅']
      ? Trees['length']
      : T extends [...infer TreesOrSanta, '🎄']
        ? FindSantaInRow<TreesOrSanta>
        : never

type FindSanta<T extends unknown[][]> = T extends '🎄'[][]
  ? never
  : T extends [('🎅' | '🎄')[], ...'🎄'[][]]
    ? [0, FindSantaInRow<T[0]>]
    : T extends [...infer First, '🎄'[]]
      ? First extends unknown[][]
        ? FindSanta<First>
        : never
      : T extends [...infer Trees, ('🎅' | '🎄')[]]
        ? [Trees['length'], FindSantaInRow<T[Trees['length']]>]
        : never

// prettier-ignore
type Forest0 = [
    ['🎅', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
];
type test_0_actual = FindSanta<Forest0>

type test_0_expected = [0, 0]
type test_0 = Expect<Equal<test_0_expected, test_0_actual>>

// prettier-ignore
type Forest1 = [
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎅', '🎄', '🎄'],
];
type test_1_actual = FindSanta<Forest1>

type test_1_expected = [3, 1]
type test_1 = Expect<Equal<test_1_expected, test_1_actual>>

// prettier-ignore
type Forest2 = [
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎅', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
];
type test_2_actual = FindSanta<Forest2>

type test_2_expected = [2, 2]
type test_2 = Expect<Equal<test_2_expected, test_2_actual>>

// prettier-ignore
type Forest3 = [
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎅', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
];
type test_3_actual = FindSanta<Forest3>

type test_3_expected = [2, 1]
type test_3 = Expect<Equal<test_3_expected, test_3_actual>>

// prettier-ignore
type Forest4 = [
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎅', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
];
type test_4_actual = FindSanta<Forest4>

type test_4_expected = [1, 2]
type test_4 = Expect<Equal<test_4_expected, test_4_actual>>
// prettier-ignore
type Forest5 = [
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
    ['🎄', '🎄', '🎄', '🎄'],
];
type test_5_actual = FindSanta<Forest5>

type test_5_expected = never
type test_5 = Expect<Equal<test_5_expected, test_5_actual>>
